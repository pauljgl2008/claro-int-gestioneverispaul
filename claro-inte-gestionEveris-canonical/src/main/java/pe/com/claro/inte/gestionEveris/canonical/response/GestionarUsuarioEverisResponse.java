package pe.com.claro.inte.gestionEveris.canonical.response;

import javax.validation.constraints.NotNull;

import pe.com.claro.common.property.ConstantesValidacion;
import pe.com.claro.inte.gestionEveris.canonical.types.GestionarUsuarioEverisResponseType;

public class GestionarUsuarioEverisResponse {

	@NotNull(message = ConstantesValidacion.NOT_NULL)
	private GestionarUsuarioEverisResponseType gestionarUsuarioEverisResponse;

	public GestionarUsuarioEverisResponseType getGestionarUsuarioEverisResponse() {
		return gestionarUsuarioEverisResponse;
	}

	public void setGestionarUsuarioEverisResponse(GestionarUsuarioEverisResponseType gestionarUsuarioEverisResponse) {
		this.gestionarUsuarioEverisResponse = gestionarUsuarioEverisResponse;
	}

}
