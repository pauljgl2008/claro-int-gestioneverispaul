package pe.com.claro.inte.gestionEveris.domain.service;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.apache.log4j.Logger;

import pe.com.claro.common.bean.HeaderRequest;
import pe.com.claro.common.exception.DBException;
import pe.com.claro.common.property.Constantes;
import pe.com.claro.common.resource.exception.WSException;
import pe.com.claro.common.util.PropertiesExternos;
import pe.com.claro.eai.ws.baseschema.AuditRequestType;
import pe.com.claro.eai.ws.baseschema.ParametrosType;
import pe.com.claro.eai.ws.consultaspostpagows.types.DatosClienteRequest;
import pe.com.claro.eai.ws.consultaspostpagows.types.DatosClienteResponse;
import pe.com.claro.inte.gestionEveris.canonical.response.GestionarUsuarioEverisResponse;
import pe.com.claro.inte.gestionEveris.canonical.types.GestionarUsuarioEverisRequestType;
import pe.com.claro.inte.gestionEveris.canonical.types.GestionarUsuarioEverisResponseType;
import pe.com.claro.inte.gestionEveris.canonical.types.ResponseAuditType;
import pe.com.claro.inte.gestionEveris.domain.repository.TimdevRepository;
import pe.com.claro.inte.gestionEveris.domain.repository.bean.SpIotawssDispasoc;
import pe.com.claro.inte.gestionEveris.integration.client.interfaz.ClaroPostConsultaPostPago;

@Stateless
public class GestionarUsuarioEverisService implements Serializable {

	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(this.getClass().getName());

	@Context
	private Configuration configuration;

	@EJB
	private ClaroPostConsultaPostPago claroPostConsultaPostPago;

	@EJB
	private TimdevRepository timdevRepository;

	public GestionarUsuarioEverisResponseType gestionarUsuarioEveris(String mensajeTransaccion,
			HeaderRequest headerRequest, GestionarUsuarioEverisRequestType request, PropertiesExternos prop)
			throws DBException {

		GestionarUsuarioEverisResponseType response = new GestionarUsuarioEverisResponseType();

		GestionarUsuarioEverisResponse gestionarUsuarioEverisResponse = new GestionarUsuarioEverisResponse();

		boolean actividad2 = false;
		boolean actividad3 = false;
		boolean actividad4 = false;

		try {

			logger.info(Constantes.INICIO_ACTIVIDAD + Constantes.ACTIVIDAD1_G + Constantes.CHAR_CORCHETE_DERECHO);
			logger.info(Constantes.SEPARADOR);

			DatosClienteResponse respuesta = new DatosClienteResponse();
			DatosClienteRequest datosRequest = new DatosClienteRequest();

			datosRequest = construirDatosClienteRequest(headerRequest, request, prop);

			respuesta = claroPostConsultaPostPago.consultarDatosCliente(mensajeTransaccion, datosRequest, headerRequest,
					prop);
			if (respuesta.getAuditResponse().getCodigoRespuesta().equals(Constantes.ZERO)) {
				actividad2 = true;
			} else {
				ResponseAuditType responseAuditType = new ResponseAuditType();
				responseAuditType.setIdTransaccion(headerRequest.getIdTransaccion());
				responseAuditType.setCodigoRespuesta(prop.resultadoWSIDF5Codigo);
				responseAuditType.setMensajeRespuesta(prop.resultadoWSIDF5Mensaje);
				response.setResponseAudit(responseAuditType);
			}
			logger.info(Constantes.SEPARADOR);
			logger.info(Constantes.FIN_ACTIVIDAD + Constantes.ACTIVIDAD1_G + Constantes.CHAR_CORCHETE_DERECHO);

			if (actividad2) {
				logger.info(Constantes.INICIO_ACTIVIDAD + Constantes.ACTIVIDAD2_G + Constantes.CHAR_CORCHETE_DERECHO);
				logger.info(Constantes.SEPARADOR);
				SpIotawssDispasoc spIotawssDispasoc = new SpIotawssDispasoc();
				spIotawssDispasoc = obtenerDispositivosAsociados(mensajeTransaccion, request, prop);
				if (spIotawssDispasoc.getpCodRpta() == Constantes.NUM_ZERO
						&& spIotawssDispasoc.getpCursorDispasoc().size() > Constantes.NUM_ZERO
						&& !spIotawssDispasoc.getpCursorDispasoc().isEmpty()
						&& spIotawssDispasoc.getpCursorEquip().size() > Constantes.NUM_ZERO
						&& !spIotawssDispasoc.getpCursorEquip().isEmpty()) {
					actividad3 = true;
				} else {
					ResponseAuditType responseAuditType = new ResponseAuditType();
					responseAuditType.setIdTransaccion(headerRequest.getIdTransaccion());
					responseAuditType.setCodigoRespuesta(prop.resultadoSPIDF1Codigo);
					responseAuditType.setMensajeRespuesta(prop.resultadoSPIDF1Mensaje);
					response.setResponseAudit(responseAuditType);
				}
				logger.info(Constantes.SEPARADOR);
				logger.info(Constantes.FIN_ACTIVIDAD + Constantes.ACTIVIDAD2_G + Constantes.CHAR_CORCHETE_DERECHO);
			}

		} catch (WSException e) {
			logger.error(e, e);
			ResponseAuditType responseAuditType = new ResponseAuditType();
			responseAuditType.setIdTransaccion(headerRequest.getIdTransaccion());
			responseAuditType.setCodigoRespuesta(e.getCode());
			responseAuditType.setMensajeRespuesta(e.getMessage());
			response.setResponseAudit(responseAuditType);
		}

		return response;

	}

	private DatosClienteRequest construirDatosClienteRequest(HeaderRequest headerRequest,
			GestionarUsuarioEverisRequestType request, PropertiesExternos prop) {

		DatosClienteRequest datosClienteRequest = new DatosClienteRequest();
		AuditRequestType auditRequestType = new AuditRequestType();
		ParametrosType parametrosType = new ParametrosType();

		auditRequestType.setIdTransaccion(headerRequest.getIdTransaccion());
		auditRequestType.setNombreAplicacion(prop.consultaPostPagoNombreAplicacion);
		auditRequestType.setIpAplicacion(prop.consultaPostPagoIpAplicacion);
		auditRequestType.setUsuarioAplicacion(headerRequest.getUserId());

		datosClienteRequest.setAuditRequest(auditRequestType);
		datosClienteRequest.setCustcode(prop.consultarDatosClienteCustCode);
		datosClienteRequest.setDnnum(request.getNumeroLinea());
		datosClienteRequest.setListaRequestOpcional(parametrosType);

		return datosClienteRequest;

	}

	private SpIotawssDispasoc obtenerDispositivosAsociados(String mensajeTransaccion,
			GestionarUsuarioEverisRequestType request, PropertiesExternos propertiesExternos) throws DBException {
		return timdevRepository.obtenerDispositivosAsociados(request.getNumeroLinea(), Constantes.EMPTY,
				mensajeTransaccion, propertiesExternos);
	}

}
