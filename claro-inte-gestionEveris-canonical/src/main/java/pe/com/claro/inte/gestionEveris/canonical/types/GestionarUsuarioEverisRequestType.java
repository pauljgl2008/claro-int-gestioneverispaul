package pe.com.claro.inte.gestionEveris.canonical.types;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import pe.com.claro.common.property.ConstantesValidacion;

public class GestionarUsuarioEverisRequestType {

	@NotNull(message = ConstantesValidacion.NOT_NULL)
	@Size(min = 1, max = 50, message = ConstantesValidacion.SIZE)
	private String numeroLinea;
	@NotNull(message = ConstantesValidacion.NOT_NULL)
	@Size(min = 1, max = 50, message = ConstantesValidacion.SIZE)
	private String codEmpleado;
	@Size(min = 1, max = 8, message = ConstantesValidacion.SIZE)
	private String nroDni;
	@NotNull(message = ConstantesValidacion.NOT_NULL)
	@Size(min = 1, max = 20, message = ConstantesValidacion.SIZE)
	private String universidadOrigen;
	private ListaOpcionalType listaOpcional;

	public String getNumeroLinea() {
		return numeroLinea;
	}

	public void setNumeroLinea(String numeroLinea) {
		this.numeroLinea = numeroLinea;
	}

	public String getCodEmpleado() {
		return codEmpleado;
	}

	public void setCodEmpleado(String codEmpleado) {
		this.codEmpleado = codEmpleado;
	}

	public String getNroDni() {
		return nroDni;
	}

	public void setNroDni(String nroDni) {
		this.nroDni = nroDni;
	}

	public String getUniversidadOrigen() {
		return universidadOrigen;
	}

	public void setUniversidadOrigen(String universidadOrigen) {
		this.universidadOrigen = universidadOrigen;
	}

	public ListaOpcionalType getListaOpcional() {
		return listaOpcional;
	}

	public void setListaOpcional(ListaOpcionalType listaOpcional) {
		this.listaOpcional = listaOpcional;
	}

}
