package pe.com.claro.inte.gestionEveris.domain.repository.bean;

import java.io.Serializable;
import java.util.List;

public class SpIotawssDispasoc implements Serializable {
	private static final long serialVersionUID = 1L;
	private int pCodRpta;
	private List<SnCurEquip> pCursorEquip;
	private List<SnCurDispasoc> pCursorDispasoc;
	private String pMsjRpta;

	public int getpCodRpta() {
		return pCodRpta;
	}

	public void setpCodRpta(int pCodRpta) {
		this.pCodRpta = pCodRpta;
	}

	public List<SnCurEquip> getpCursorEquip() {
		return pCursorEquip;
	}

	public void setpCursorEquip(List<SnCurEquip> pCursorEquip) {
		this.pCursorEquip = pCursorEquip;
	}

	public List<SnCurDispasoc> getpCursorDispasoc() {
		return pCursorDispasoc;
	}

	public void setpCursorDispasoc(List<SnCurDispasoc> pCursorDispasoc) {
		this.pCursorDispasoc = pCursorDispasoc;
	}

	public String getpMsjRpta() {
		return pMsjRpta;
	}

	public void setpMsjRpta(String pMsjRpta) {
		this.pMsjRpta = pMsjRpta;
	}

}
