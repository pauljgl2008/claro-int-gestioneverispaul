package pe.com.claro.inte.gestionEveris.canonical.types;

public class ResponseDataType {

	private ListaOpcionalType listaOpcional;

	public ListaOpcionalType getListaOpcional() {
		return listaOpcional;
	}

	public void setListaOpcional(ListaOpcionalType listaOpcional) {
		this.listaOpcional = listaOpcional;
	}

}
