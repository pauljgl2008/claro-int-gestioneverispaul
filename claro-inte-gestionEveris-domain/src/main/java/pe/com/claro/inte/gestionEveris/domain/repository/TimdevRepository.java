package pe.com.claro.inte.gestionEveris.domain.repository;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.dialect.OracleTypesHelper;
import org.hibernate.jdbc.Work;

import oracle.jdbc.OracleCallableStatement;
import pe.com.claro.common.exception.DBException;
import pe.com.claro.common.property.Constantes;
import pe.com.claro.common.property.JAXBUtilitarios;
import pe.com.claro.common.util.PropertiesExternos;
import pe.com.claro.inte.gestionEveris.domain.repository.bean.SnCurDispasoc;
import pe.com.claro.inte.gestionEveris.domain.repository.bean.SnCurEquip;
import pe.com.claro.inte.gestionEveris.domain.repository.bean.SpIotawssDispasoc;

@Stateless
public class TimdevRepository implements Serializable {

	private static final long serialVersionUID = 1L;
	private EntityManager entityManager;
	private static final Logger logger = Logger.getLogger(TimdevRepository.class);

	@PersistenceContext(unitName = Constantes.PERSISTENCE_CONTEXT_TIMDEV)
	public void setPersistenceUnit00(final EntityManager em) {
		this.entityManager = em;
		logger.info("Cargando el contexto TimdevRepository");
	}

	public SpIotawssDispasoc obtenerDispositivosAsociados(String piMisisdnEquip, String piImeiEquip,
			String mensajeTransaccionSCS, PropertiesExternos propertiesExternos) throws DBException {

		logger.info(mensajeTransaccionSCS + " ====== [Inicio] En BSCS IOTAWSS_DISPASOC ====== ");
		long iniciotiempo = System.currentTimeMillis();
		final String owner = propertiesExternos.dbTimDevOwner;
		String procedure = null;
		String bd = null;
		bd = propertiesExternos.dbTimDevNombre;
		procedure = propertiesExternos.dbTimDevPkgHubAw + Constantes.SEPARADORPUNTO
				+ propertiesExternos.dbTimDevSpIotawssDispasoc;
		logger.info(
				mensajeTransaccionSCS + Constantes.TEXTO_USREAIDESA + Constantes.TIMDEV_USREAIDESA + Constantes.TEXTOVACIO);
		logger.info(mensajeTransaccionSCS + Constantes.TEXTOOWNER + owner + Constantes.TEXTO_VACIO);
		logger.info(mensajeTransaccionSCS + Constantes.TEXTOPAQUETE + procedure + Constantes.TEXTO_VACIO);

		StringBuffer storedProcedureSBSCS = new StringBuffer();
		storedProcedureSBSCS.append(owner);
		storedProcedureSBSCS.append(Constantes.SEPARADORPUNTO);
		storedProcedureSBSCS.append(procedure);
		SpIotawssDispasoc response = new SpIotawssDispasoc();

		try {

			logger.info(mensajeTransaccionSCS + Constantes.CONSULTAINICIO + procedure + Constantes.TEXTO_VACIO);
			Session sessionSCS = (Session) this.entityManager.unwrap(Session.class);
			String timeoutBSCSDB = propertiesExternos.dbTimDevTimeout;
			List<SnCurEquip> pCursorEquip = new ArrayList<SnCurEquip>();
			List<SnCurDispasoc> pCursorDispasoc = new ArrayList<SnCurDispasoc>();
			

			sessionSCS.doWork(new Work() {
				@Override
				public void execute(Connection connection) throws SQLException {
					ResultSet pcursosEquipPostServicio = null;
					ResultSet pcursosDispasocPostServicio = null;
					OracleCallableStatement cst = (OracleCallableStatement) connection
							.prepareCall(Constantes.CALL + storedProcedureSBSCS.toString() + "(?,?,?,?,?,?)");
					cst.setQueryTimeout(Integer.parseInt(timeoutBSCSDB));
					cst.setString(1, piMisisdnEquip);
					cst.setString(2, piImeiEquip);
					logger.info(mensajeTransaccionSCS + Constantes.PARAMINPUT
							+ (Constantes.P_COD_PROD + Constantes.CHAR_CORCHETE_PUNTOS + piMisisdnEquip + Constantes.COMA
									+ piImeiEquip + Constantes.CHAR_CORCHETE_DERECHO));
					cst.registerOutParameter(3, Types.VARCHAR);
					cst.registerOutParameter(4, Types.VARCHAR);
					cst.registerOutParameter(5, OracleTypesHelper.INSTANCE.getOracleCursorTypeSqlType());
					cst.registerOutParameter(6, OracleTypesHelper.INSTANCE.getOracleCursorTypeSqlType());
					cst.execute();
					response.setpCodRpta(Integer.valueOf(cst.getObject(3).toString()));
					response.setpMsjRpta(cst.getObject(4).toString());
					pcursosEquipPostServicio = (ResultSet) cst.getObject(5);
					pcursosDispasocPostServicio = (ResultSet) cst.getObject(6);

					logger.info(mensajeTransaccionSCS + Constantes.PARAMOUTPUT + Constantes.P_CURSOR_EQUIP
							+ Constantes.CHAR_CORCHETE_PUNTOS);

					if (pcursosEquipPostServicio != null) {
						int i = 0;
						SnCurEquip datosCurEquip = null;
						while (pcursosEquipPostServicio.next()) {
							datosCurEquip = new SnCurEquip();

							datosCurEquip.setPoMisisdn(pcursosEquipPostServicio.getString(Constantes.PO_MSISDN_EQUIP));
							datosCurEquip.setPoIccid(pcursosEquipPostServicio.getString(Constantes.PO_ICCID_EQUIP));
							datosCurEquip.setPoImsi(pcursosEquipPostServicio.getString(Constantes.PO_IMSI_EQUIP));
							datosCurEquip.setPoPlan(pcursosEquipPostServicio.getString(Constantes.PO_PLAN_EQUIP));
							datosCurEquip.setPoImei(pcursosEquipPostServicio.getString(Constantes.PO_IMEI_EQUIP));
							logger.info(JAXBUtilitarios.anyObjectToXmlText(datosCurEquip));
							pCursorEquip.add(datosCurEquip);
							i++;
						}
						if (i == 0) {
							logger.info(
									mensajeTransaccionSCS + Constantes.PARAMOUTPUT + Constantes.PARAMOUTCURSOR + "=[VACIO]");
						}
					}
					response.setpCursorEquip(pCursorEquip);
					
					logger.info(mensajeTransaccionSCS + Constantes.PARAMOUTPUT + Constantes.P_CURSOR_DISPASOC
							+ Constantes.CHAR_CORCHETE_PUNTOS);

					if (pcursosDispasocPostServicio != null) {
						int i = 0;
						SnCurDispasoc datosCurDispasoc = null;
						while (pcursosDispasocPostServicio.next()) {
							datosCurDispasoc = new SnCurDispasoc();

							datosCurDispasoc
									.setPoMisisdn(pcursosDispasocPostServicio.getString(Constantes.PO_MSISDN_DISPASOC));
							datosCurDispasoc.setPoIccid(pcursosDispasocPostServicio.getString(Constantes.PO_ICCID_DISPASOC));
							datosCurDispasoc.setPoImsi(pcursosDispasocPostServicio.getString(Constantes.PO_IMSI_DISPASOC));
							datosCurDispasoc.setPoImei(pcursosDispasocPostServicio.getString(Constantes.PO_IMEI_DISPASOC));
							datosCurDispasoc.setPoEid(pcursosDispasocPostServicio.getString(Constantes.PO_EID_DISPASOC));
							datosCurDispasoc.setPoEst(pcursosDispasocPostServicio.getString(Constantes.PO_EST_DISPASOC));
							datosCurDispasoc.setPoFecActiv(pcursosDispasocPostServicio.getString(Constantes.PO_FEC_ACTIV));
							datosCurDispasoc.setPoDescServ(pcursosDispasocPostServicio.getString(Constantes.PO_DESC_SERV));

							logger.info(JAXBUtilitarios.anyObjectToXmlText(datosCurDispasoc));
							pCursorDispasoc.add(datosCurDispasoc);
							i++;
						}
						if (i == 0) {
							logger.info(
									mensajeTransaccionSCS + Constantes.PARAMOUTPUT + Constantes.PARAMOUTCURSOR + "=[VACIO]");
						}
					}
					response.setpCursorDispasoc(pCursorDispasoc);

					logger.info(mensajeTransaccionSCS + Constantes.PARAMOUTPUT + Constantes.P_RESULTADO
							+ Constantes.DOSPUNTOS + cst.getObject(3).toString() + Constantes.TEXTO_VACIO);
					logger.info(mensajeTransaccionSCS + Constantes.PARAMOUTPUT + Constantes.P_MSGERR + Constantes.DOSPUNTOS
							+ cst.getObject(4).toString() + Constantes.TEXTO_VACIO);
				}

			});

			logger.info(mensajeTransaccionSCS + Constantes.CONSULTAEXITO);

		} catch (Exception e) {
			logger.error(mensajeTransaccionSCS + Constantes.EXCEPTIONTEXT + bd, e);
			if (JAXBUtilitarios.getErrorTrace(e).toUpperCase(Locale.getDefault())
					.contains(Constantes.SQLTIMEOUTEXCEPTION)) {
				logger.info(mensajeTransaccionSCS + Constantes.TEXTOERRORTIMEOUT
						+ propertiesExternos.procedureMensajeErrorIdt1
								.replace(Constantes.NOMBRESP, storedProcedureSBSCS.toString())
								.replace(Constantes.NOMBREJNDI, Constantes.BSCSJNDI));
				throw new DBException(propertiesExternos.procedureCodigoErrorIdt1,
						propertiesExternos.procedureMensajeErrorIdt1
								.replace(Constantes.NOMBRESP, storedProcedureSBSCS.toString())
								.replace(Constantes.NOMBREJNDI, Constantes.TIMDEV_USREAIDESA));
			} else {
				logger.info(mensajeTransaccionSCS + Constantes.TEXTOERRORDISPONIBILIDAD
						+ propertiesExternos.procedureMensajeErrorIdt2
								.replace(Constantes.NOMBRESP, storedProcedureSBSCS.toString())
								.replace(Constantes.NOMBREJNDI, Constantes.TIMDEV_USREAIDESA));
				throw new DBException(propertiesExternos.procedureCodigoErrorIdt2,
						propertiesExternos.procedureMensajeErrorIdt2
								.replace(Constantes.NOMBRESP, storedProcedureSBSCS.toString())
								.replace(Constantes.NOMBREJNDI, Constantes.TIMDEV_USREAIDESA));
			}
		} finally {
			logger.info(mensajeTransaccionSCS + Constantes.TEXTOESPACIO + Constantes.TIEMPOTOTALPROCESO
					+ Constantes.CHAR_CORCHETE_PUNTOS + (System.currentTimeMillis() - iniciotiempo)
					+ Constantes.MILISEGUNDOS);
			logger.info(mensajeTransaccionSCS + " ====== [Fin] En TIMDEV SP_IOTAWSS_DISPASOC ====== ");
		}

		return response;

	}
}
