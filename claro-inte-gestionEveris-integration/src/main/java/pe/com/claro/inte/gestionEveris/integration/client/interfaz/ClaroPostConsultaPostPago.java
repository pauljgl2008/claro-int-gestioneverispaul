package pe.com.claro.inte.gestionEveris.integration.client.interfaz;

import pe.com.claro.common.bean.HeaderRequest;
import pe.com.claro.common.resource.exception.WSException;
import pe.com.claro.common.util.PropertiesExternos;
import pe.com.claro.eai.ws.consultaspostpagows.types.DatosClienteRequest;
import pe.com.claro.eai.ws.consultaspostpagows.types.DatosClienteResponse;



public interface ClaroPostConsultaPostPago {
	
	public DatosClienteResponse consultarDatosCliente(String msjTx, DatosClienteRequest request, 
			HeaderRequest header, PropertiesExternos prop) throws WSException;

}
