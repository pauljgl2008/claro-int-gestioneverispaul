package pe.com.claro.inte.gestionEveris.domain.repository.bean;

import java.io.Serializable;

public class SnCurDispasoc implements Serializable {

	private static final long serialVersionUID = 1L;
	private String poMisisdn;
	private String poIccid;
	private String poImsi;
	private String poImei;
	private String poEid;
	private String poEst;
	private String poFecActiv;
	private String poDescServ;

	public String getPoMisisdn() {
		return poMisisdn;
	}

	public void setPoMisisdn(String poMisisdn) {
		this.poMisisdn = poMisisdn;
	}

	public String getPoIccid() {
		return poIccid;
	}

	public void setPoIccid(String poIccid) {
		this.poIccid = poIccid;
	}

	public String getPoImsi() {
		return poImsi;
	}

	public void setPoImsi(String poImsi) {
		this.poImsi = poImsi;
	}

	public String getPoImei() {
		return poImei;
	}

	public void setPoImei(String poImei) {
		this.poImei = poImei;
	}

	public String getPoEid() {
		return poEid;
	}

	public void setPoEid(String poEid) {
		this.poEid = poEid;
	}

	public String getPoEst() {
		return poEst;
	}

	public void setPoEst(String poEst) {
		this.poEst = poEst;
	}

	public String getPoFecActiv() {
		return poFecActiv;
	}

	public void setPoFecActiv(String poFecActiv) {
		this.poFecActiv = poFecActiv;
	}

	public String getPoDescServ() {
		return poDescServ;
	}

	public void setPoDescServ(String poDescServ) {
		this.poDescServ = poDescServ;
	}

}
