package pe.com.claro.inte.gestionEveris.domain.repository.bean;

import java.io.Serializable;

public class SnCurEquip implements Serializable {

	private static final long serialVersionUID = 1L;

	private String poMisisdn;
	private String poIccid;
	private String poImsi;
	private String poPlan;
	private String poImei;

	public String getPoMisisdn() {
		return poMisisdn;
	}

	public void setPoMisisdn(String poMisisdn) {
		this.poMisisdn = poMisisdn;
	}

	public String getPoIccid() {
		return poIccid;
	}

	public void setPoIccid(String poIccid) {
		this.poIccid = poIccid;
	}

	public String getPoImsi() {
		return poImsi;
	}

	public void setPoImsi(String poImsi) {
		this.poImsi = poImsi;
	}

	public String getPoPlan() {
		return poPlan;
	}

	public void setPoPlan(String poPlan) {
		this.poPlan = poPlan;
	}

	public String getPoImei() {
		return poImei;
	}

	public void setPoImei(String poImei) {
		this.poImei = poImei;
	}

}
