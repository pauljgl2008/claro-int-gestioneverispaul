package pe.com.claro.common.util;

import javax.ws.rs.core.Configuration;

public class PropertiesExternos {

  public final String dbBscscdbNombre;
  public final String dbBscsdbOwner;
  public final String dbBscsTimeout;
  public final String dbBscsSPkgPp017SiacMigraciones;
  public final String dbBscsSPkgTim157CambioCiclo;
  public final String dbBscsSpSncodexProd;
  public final String dbBscsSpServCorexPlan;
  public final String dbBscsSpTimLiberaRecursosXCbios;
  public final String dbBscsSpValidaContrato;

  public final String procedureCodigoErrorIdt1;
  public final String procedureMensajeErrorIdt1;
  public final String procedureCodigoErrorIdt2;
  public final String procedureMensajeErrorIdt2;
  public final String idt1Codigo;
  public final String idt1Mensaje;
  public final String idt2Codigo;
  public final String idt2Mensaje;
  public final String idt3Codigo;
  public final String idt3Mensaje;
  public final String wsCodigoIdt1;
  public final String wsCodigoIdt2;
  public final String wsMensajeIdt1;
  public final String wsMensajeIdt2;

  public final String crearContratoIDF0Codigo;
  public final String crearContratoIDF0Mensaje;
  public final String crearContratoIDF1Codigo;
  public final String crearContratoIDF1Mensaje;
  public final String resultadoSPIDF1Codigo;
  public final String resultadoSPIDF1Mensaje;
  public final String resultadoSPIDF2Codigo;
  public final String resultadoSPIDF2Mensaje;
  public final String resultadoRESTIDF3Codigo;
  public final String resultadoRESTIDF3Mensaje;
  public final String resultadoSPIDF4Codigo;
  public final String resultadoSPIDF4Mensaje;
  public final String resultadoWSIDF5Codigo;
  public final String resultadoWSIDF5Mensaje;
  public final String resultadoSPIDF6Codigo;
  public final String resultadoSPIDF6Mensaje;

  public final String claroPostLineaUri;
  public final String claroPostLineaNombre;
  public final String claroPostLineaClienteNombre;
  public final String claroPostLineaConnectTimeOut;
  public final String claroPostLineaReadTimeOut;

  public final String wsActivosPostpagoURL;
  public final String wsActivosPostpagoMetodo;
  public final String wsActivosPostpagoNombre;
  public final String wsActivosPostpagoTimeoutConnect;
  public final String wsActivosPostpagoTimeoutRequest;

  public final String activosPostpagoNombreAplicacion;
  public final String activosPostpagoIpAplicacion;
  public final String activosPostpagoIdSubMercado;
  public final String activosPostpagoIdMercado;
  public final String activosPostpagoRed;
  public final String activosPostpagoCantidadUmbral;
  public final String activosPostpagoArchivoLlamadas;
  public final String activosPostpagoProfileId;
  public final String activosPostpagoTipoCostoServicioAvanzado;
  public final String activosPostpagoTipoCostoServicio;
  public final String activosPostpagoEstado;
  
  public final String consultaPostPagoUri;
  public final String consultaPostPagoNombre;
  public final String consultaPostPagoMetodo;
  public final String consultaPostPagoTimeoutConnect;
  public final String consultaPostPagoTimeoutRequest;
  public final String consultaPostPagoIpAplicacion;
  public final String consultaPostPagoNombreAplicacion;
  public final String consultarDatosClienteCustCode;
  public final String dbTimDevOwner;
  public final String dbTimDevNombre;
  public final String dbTimDevPkgHubAw;
  public final String dbTimDevSpIotawssDispasoc;
  public final String dbTimDevTimeout;

  public PropertiesExternos(Configuration config) {
    super();
    this.dbBscscdbNombre = ClaroUtil.convertProp(config.getProperty("db.bscscdb.nombre"));
    this.dbBscsdbOwner = ClaroUtil.convertProp(config.getProperty("db.bscscdb.owner"));
    this.dbBscsTimeout = ClaroUtil.convertProp(config.getProperty("db.bscscdb.timeout"));
    this.dbBscsSPkgPp017SiacMigraciones = ClaroUtil
        .convertProp(config.getProperty("db.bscscdb.pkg.pp017siacmigraciones"));
    this.dbBscsSPkgTim157CambioCiclo = ClaroUtil.convertProp(config.getProperty("db.bscscdb.pkg.tim157cambioCiclo"));
    this.dbBscsSpSncodexProd = ClaroUtil.convertProp(config.getProperty("db.bscscdb.sp.sncodexprod"));
    this.dbBscsSpServCorexPlan = ClaroUtil.convertProp(config.getProperty("db.bscscdb.sp.servcorexplan"));
    this.dbBscsSpTimLiberaRecursosXCbios = ClaroUtil
        .convertProp(config.getProperty("db.bscscdb.sp.timliberarecursosxcbios"));
    this.dbBscsSpValidaContrato = ClaroUtil.convertProp(config.getProperty("db.bscscdb.sp.validacontrato"));

    this.procedureCodigoErrorIdt1 = ClaroUtil.convertProp(config.getProperty("procedure.codigo.error.idt1"));
    this.procedureMensajeErrorIdt1 = ClaroUtil.convertProp(config.getProperty("procedure.mensaje.error.idt1"));
    this.procedureCodigoErrorIdt2 = ClaroUtil.convertProp(config.getProperty("procedure.codigo.error.idt2"));
    this.procedureMensajeErrorIdt2 = ClaroUtil.convertProp(config.getProperty("procedure.mensaje.error.idt2"));
    this.idt1Codigo = ClaroUtil.convertProp(config.getProperty("idt1.codigo"));
    this.idt1Mensaje = ClaroUtil.convertProp(config.getProperty("idt1.mensaje"));
    this.idt2Codigo = ClaroUtil.convertProp(config.getProperty("idt2.codigo"));
    this.idt2Mensaje = ClaroUtil.convertProp(config.getProperty("idt2.mensaje"));
    this.idt3Codigo = ClaroUtil.convertProp(config.getProperty("idt3.codigo"));
    this.idt3Mensaje = ClaroUtil.convertProp(config.getProperty("idt3.mensaje"));
    this.wsCodigoIdt1 = ClaroUtil.convertProp(config.getProperty("ws.codigo.idt1"));
    this.wsCodigoIdt2 = ClaroUtil.convertProp(config.getProperty("ws.codigo.idt2"));
    this.wsMensajeIdt1 = ClaroUtil.convertProp(config.getProperty("ws.mensaje.idt1"));
    this.wsMensajeIdt2 = ClaroUtil.convertProp(config.getProperty("ws.mensaje.idt2"));

    this.crearContratoIDF0Codigo = ClaroUtil.convertProp(config.getProperty("crearcontrato.idf0.codigo"));
    this.crearContratoIDF0Mensaje = ClaroUtil.convertProp(config.getProperty("crearcontrato.idf0.mensaje"));
    this.crearContratoIDF1Codigo = ClaroUtil.convertProp(config.getProperty("crearcontrato.idf1.codigo"));
    this.crearContratoIDF1Mensaje = ClaroUtil.convertProp(config.getProperty("crearcontrato.idf1.mensaje"));
    this.resultadoSPIDF1Codigo = ClaroUtil.convertProp(config.getProperty("resultado.sp.idf1.codigo"));
    this.resultadoSPIDF1Mensaje = ClaroUtil.convertProp(config.getProperty("resultado.sp.idf1.mensaje"));
    this.resultadoSPIDF2Codigo = ClaroUtil.convertProp(config.getProperty("resultado.sp.idf2.codigo"));
    this.resultadoSPIDF2Mensaje = ClaroUtil.convertProp(config.getProperty("resultado.sp.idf2.mensaje"));
    this.resultadoRESTIDF3Codigo = ClaroUtil.convertProp(config.getProperty("resultado.rest.idf3.codigo"));
    this.resultadoRESTIDF3Mensaje = ClaroUtil.convertProp(config.getProperty("resultado.rest.idf3.mensaje"));
    this.resultadoSPIDF4Codigo = ClaroUtil.convertProp(config.getProperty("resultado.sp.idf4.codigo"));
    this.resultadoSPIDF4Mensaje = ClaroUtil.convertProp(config.getProperty("resultado.sp.idf4.mensaje"));
    this.resultadoWSIDF5Codigo = ClaroUtil.convertProp(config.getProperty("resultado.ws.idf5.codigo"));
    this.resultadoWSIDF5Mensaje = ClaroUtil.convertProp(config.getProperty("resultado.ws.idf5.mensaje"));
    this.resultadoSPIDF6Codigo = ClaroUtil.convertProp(config.getProperty("resultado.sp.idf6.codigo"));
    this.resultadoSPIDF6Mensaje = ClaroUtil.convertProp(config.getProperty("resultado.sp.idf6.mensaje"));

    this.claroPostLineaUri = ClaroUtil.convertProp(config.getProperty("claropostlinea.consultar.uri"));
    this.claroPostLineaNombre = ClaroUtil.convertProp(config.getProperty("claropostlinea.nombre"));
    this.claroPostLineaClienteNombre = ClaroUtil.convertProp(config.getProperty("claropostlinea.nombre.metodo"));
    this.claroPostLineaConnectTimeOut = ClaroUtil.convertProp(config.getProperty("claropostlinea.connect.timeout"));
    this.claroPostLineaReadTimeOut = ClaroUtil.convertProp(config.getProperty("claropostlinea.read.timeout"));

    this.wsActivosPostpagoURL = ClaroUtil.convertProp(config.getProperty("ws.activospostpagows.url"));
    this.wsActivosPostpagoMetodo = ClaroUtil.convertProp(config.getProperty("ws.activospostpagows.metodo"));
    this.wsActivosPostpagoNombre = ClaroUtil.convertProp(config.getProperty("ws.activospostpagows.nombre"));
    this.wsActivosPostpagoTimeoutConnect = ClaroUtil
        .convertProp(config.getProperty("ws.activospostpagows.timeout.connect"));
    this.wsActivosPostpagoTimeoutRequest = ClaroUtil
        .convertProp(config.getProperty("ws.activospostpagows.timeout.request"));

    this.activosPostpagoNombreAplicacion = ClaroUtil
        .convertProp(config.getProperty("activospostpago.nombre.aplicacion"));
    this.activosPostpagoIpAplicacion = ClaroUtil.convertProp(config.getProperty("activospostpago.ip.aplicacion"));
    this.activosPostpagoIdSubMercado = ClaroUtil.convertProp(config.getProperty("activospostpago.id.submercado"));
    this.activosPostpagoIdMercado = ClaroUtil.convertProp(config.getProperty("activospostpago.id.mercado"));
    this.activosPostpagoRed = ClaroUtil.convertProp(config.getProperty("activospostpago.red"));
    this.activosPostpagoCantidadUmbral = ClaroUtil.convertProp(config.getProperty("activospostpago.cantidad.umbral"));
    this.activosPostpagoArchivoLlamadas = ClaroUtil.convertProp(config.getProperty("activosPostpago.archivo.llamadas"));
    this.activosPostpagoProfileId = ClaroUtil.convertProp(config.getProperty("activospostpago.profileid"));
    this.activosPostpagoTipoCostoServicioAvanzado = ClaroUtil
        .convertProp(config.getProperty("activospostpago.tipocosto.servicioavanzado"));
    this.activosPostpagoTipoCostoServicio = ClaroUtil
        .convertProp(config.getProperty("activospostpago.tipocostoservicio"));
    this.activosPostpagoEstado = ClaroUtil.convertProp(config.getProperty("activospostpago.estado"));
    
    this.consultaPostPagoUri = ClaroUtil.convertProp(config.getProperty("claropostpago.consultar.uri"));
    this.consultaPostPagoNombre = ClaroUtil.convertProp(config.getProperty("claropostpago.nombre"));
    this.consultaPostPagoMetodo = ClaroUtil.convertProp(config.getProperty("claropostpago.nombre.metodo"));    
    this.consultaPostPagoTimeoutConnect = ClaroUtil
          .convertProp(config.getProperty("claropostpago.timeout.connect"));
    this.consultaPostPagoTimeoutRequest = ClaroUtil
          .convertProp(config.getProperty("claropostpago.timeout.request"));
    this.consultaPostPagoIpAplicacion = ClaroUtil.convertProp(config.getProperty("consultapostpago.ip.aplicacion"));
    this.consultaPostPagoNombreAplicacion = ClaroUtil
          .convertProp(config.getProperty("consultapostpago.nombre.aplicacion"));    
    this.consultarDatosClienteCustCode = ClaroUtil.convertProp("consultardatoscliente.cuscode");
    
    this.dbTimDevNombre = ClaroUtil.convertProp(config.getProperty("db.timdevdb.nombre"));
    this.dbTimDevOwner = ClaroUtil.convertProp(config.getProperty("db.timdevdb.owner"));
    this.dbTimDevPkgHubAw = ClaroUtil
          .convertProp(config.getProperty("db.timdevdb.pkg.hubaw"));
    this.dbTimDevSpIotawssDispasoc = ClaroUtil.convertProp(config.getProperty("db.timdevdb.sp.iotawssdispasoc"));
    this.dbTimDevTimeout = ClaroUtil.convertProp(config.getProperty("db.timdevdb.timeout"));
    
  }
}