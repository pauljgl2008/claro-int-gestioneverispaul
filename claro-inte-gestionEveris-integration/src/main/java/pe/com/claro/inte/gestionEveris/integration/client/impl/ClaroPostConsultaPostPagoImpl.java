package pe.com.claro.inte.gestionEveris.integration.client.impl;

import javax.ejb.Stateless;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;
import org.springframework.remoting.jaxws.JaxWsSoapFaultException;

import pe.com.claro.common.bean.HeaderRequest;
import pe.com.claro.common.property.Constantes;
import pe.com.claro.common.property.JAXBUtilitarios;
import pe.com.claro.common.resource.exception.WSException;
import pe.com.claro.common.util.PropertiesExternos;
import pe.com.claro.eai.ws.consultaspostpagows.ConsultasPostpagoWSPortType;
import pe.com.claro.eai.ws.consultaspostpagows.ConsultasPostpagoWSService;
import pe.com.claro.eai.ws.consultaspostpagows.types.DatosClienteRequest;
import pe.com.claro.eai.ws.consultaspostpagows.types.DatosClienteResponse;
import pe.com.claro.inte.gestionEveris.integration.client.interfaz.ClaroPostConsultaPostPago;

@Stateless
public class ClaroPostConsultaPostPagoImpl implements ClaroPostConsultaPostPago {

	private static transient Logger logger = Logger.getLogger(ClaroPostConsultaPostPagoImpl.class);

	@Override
	public DatosClienteResponse consultarDatosCliente(String msjTx, DatosClienteRequest request,
			HeaderRequest header, PropertiesExternos prop) throws WSException {

		String metodo = "consultarDatosCliente";
		logger.info(msjTx + Constantes.INICIO_METODO + metodo);
		DatosClienteResponse response = new DatosClienteResponse();
		long tiempoInicio = System.currentTimeMillis();
		String url = prop.consultaPostPagoUri;

		try {
			
			logger.info(" URL del Servicio a invocar: " + url);
	      logger.info(" Timeout Connect: " + prop.consultaPostPagoTimeoutConnect);
	      logger.info(" Timeout Request: " + prop.consultaPostPagoTimeoutRequest);
	      logger.info(" Datos de entrada: " + JAXBUtilitarios.anyObjectToXmlText(request));
	      
	      ConsultasPostpagoWSService service =  new ConsultasPostpagoWSService();
	      
	      ConsultasPostpagoWSPortType port = service.getConsultasPostpagoWSSB11();
			
	      BindingProvider bindingProvider = (BindingProvider) port;
	      bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, prop.consultaPostPagoUri);
	      bindingProvider.getRequestContext().put(Constantes.CONNECT_TIMEOUT,
	          Integer.parseInt(prop.consultaPostPagoTimeoutConnect));
	      bindingProvider.getRequestContext().put(Constantes.REQUEST_TIMEOUT,
	          Integer.parseInt(prop.consultaPostPagoTimeoutRequest));
	      
	      response = port.consultarDatosCliente(request);
	      
	      logger.info(" Datos de salida: " + JAXBUtilitarios.anyObjectToXmlText(response));

		} catch (JaxWsSoapFaultException e) {
			String codResp = null;
			String msjResp = null;

			logger.error("Error devuelto: " + e.getFault().getDetail());
			logger.error("Error JaxWsSoapFaultException: " + e, e);

			codResp = prop.idt3Codigo;
			msjResp = String.format(prop.idt3Mensaje, prop.consultaPostPagoUri);

			throw new WSException(codResp, msjResp, e);
		} catch (Exception e) {
			logger.error(e, e);
			String error = (e + Constantes.TEXTO_VACIO);

			if (error.contains(Constantes.TIMEOUT)) {
				throw new WSException(prop.wsCodigoIdt1,
						String.format(prop.wsMensajeIdt1, prop.consultaPostPagoNombre, prop.consultaPostPagoMetodo), e);

			} else {
				throw new WSException(prop.wsCodigoIdt2,
						String.format(prop.wsMensajeIdt2, prop.consultaPostPagoNombre, prop.consultaPostPagoMetodo), e);
			}

		} finally {
			long tiempofinal = System.currentTimeMillis() - tiempoInicio;
			logger.info(msjTx + Constantes.FIN_METODO + metodo + Constantes.TIEMPO_TOTAL + tiempofinal);
		}

		return response;

	}

}
