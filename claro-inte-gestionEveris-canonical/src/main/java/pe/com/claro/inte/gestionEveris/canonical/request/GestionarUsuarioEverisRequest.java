package pe.com.claro.inte.gestionEveris.canonical.request;

import javax.validation.constraints.NotNull;

import pe.com.claro.common.property.ConstantesValidacion;
import pe.com.claro.inte.gestionEveris.canonical.types.GestionarUsuarioEverisRequestType;

public class GestionarUsuarioEverisRequest {

	@NotNull(message = ConstantesValidacion.NOT_NULL)
	private GestionarUsuarioEverisRequestType gestionarUsuarioEverisRequest;

	public GestionarUsuarioEverisRequestType getGestionarUsuarioEverisRequest() {
		return gestionarUsuarioEverisRequest;
	}

	public void setGestionarUsuarioEverisRequest(GestionarUsuarioEverisRequestType gestionarUsuarioEverisRequest) {
		this.gestionarUsuarioEverisRequest = gestionarUsuarioEverisRequest;
	}

}
