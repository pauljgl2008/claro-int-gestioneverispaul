package pe.com.claro.inte.gestionEveris.canonical.types;

import java.io.Serializable;

import javax.validation.constraints.Size;

import pe.com.claro.common.property.ConstantesValidacion;

public class ListaOpcionalType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Size(min = 1, max = 255, message = ConstantesValidacion.SIZE)
	private String clave;

	@Size(min = 1, max = 255, message = ConstantesValidacion.SIZE)
	private String valor;

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
