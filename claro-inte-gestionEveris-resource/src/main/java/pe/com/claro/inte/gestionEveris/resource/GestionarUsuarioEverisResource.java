package pe.com.claro.inte.gestionEveris.resource;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

import pe.com.claro.common.bean.BodyResponse;
import pe.com.claro.common.bean.HeaderRequest;
import pe.com.claro.common.property.Constantes;
import pe.com.claro.common.property.JAXBUtilitarios;
import pe.com.claro.common.util.ClaroUtil;
import pe.com.claro.common.util.PropertiesExternos;
import pe.com.claro.inte.gestionEveris.canonical.request.GestionarUsuarioEverisRequest;
import pe.com.claro.inte.gestionEveris.canonical.response.GestionarUsuarioEverisResponse;
import pe.com.claro.inte.gestionEveris.canonical.types.GestionarUsuarioEverisResponseType;
import pe.com.claro.inte.gestionEveris.canonical.types.ResponseAuditType;
import pe.com.claro.inte.gestionEveris.domain.service.GestionarUsuarioEverisService;
import pe.com.claro.inte.gestionEveris.resource.mapper.CastingMapper;

@Stateless
@Path(Constantes.PATH_GESTION)
@Consumes({ MediaType.APPLICATION_JSON })
@Api(value = Constantes.BASEPATH_GESTION, description = Constantes.PATH_RESOURCE_DESCRIPCION_GESTION)
@Produces({ MediaType.APPLICATION_JSON + Constantes.CHARSET })
public class GestionarUsuarioEverisResource {

	private static final Logger logger = Logger.getLogger(GestionarUsuarioEverisResource.class);

	@Context
	private Configuration configuration;
	private PropertiesExternos propertiesExternos;

	@EJB
	private GestionarUsuarioEverisService gestionarUsuarioEverisService;

	public void initProperties() {
		try {
			propertiesExternos = new PropertiesExternos(configuration);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON + Constantes.CHARSET })
	@ApiOperation(value = "Gestionar Usuario Everis")
	@Path(Constantes.PATH_METODO_GESTIONAR_USUARIO)
	public Response gestionarUsuarioEveris(@Context HttpHeaders httpHeaders, GestionarUsuarioEverisRequest request)
			throws JsonProcessingException {

		initProperties();
		Response resJSON = Response.ok().entity(Constantes.TEXTOVACIO).build();
		HeaderRequest headerRequest = new HeaderRequest(httpHeaders);
		String idtx = headerRequest.getIdTransaccion();
		String msgid = headerRequest.getMsgid();
		String mensajeTransaccion = Constantes.CHAR_CORCHETE_IZQUIERDO + Constantes.PATH_METODO_GESTIONAR_USUARIO
				+ Constantes.ID_TXT + idtx + Constantes.MSG_ID + msgid + Constantes.CORCHETE;
		long tiempoInicio = System.currentTimeMillis();
		String requestPrint = null;
		String responsePrint = null;
		Status httpCode = Status.OK;
		String result = Constantes.TEXTOVACIO;

		GestionarUsuarioEverisResponse response = new GestionarUsuarioEverisResponse();
		GestionarUsuarioEverisResponseType gestionarUsuarioResponse = new GestionarUsuarioEverisResponseType();
		response.setGestionarUsuarioEverisResponse(new GestionarUsuarioEverisResponseType());
		ResponseAuditType responseAudit = new ResponseAuditType();
		logger.info(mensajeTransaccion + Constantes.SEPARADOR);
		logger.info(mensajeTransaccion + Constantes.INICIO_METODO + Constantes.METODO_GESTIONAR_USUARIO);
		logger.info(mensajeTransaccion + Constantes.SEPARADOR);

		try {
			
			requestPrint = ClaroUtil.printPrettyJSONString(request);
	      logger.info(mensajeTransaccion + Constantes.PARAMETROSENTRADA);
	      logger.info(mensajeTransaccion + Constantes.PARAMETROSHEADER + Constantes.SALTO_LINEA
	          + ClaroUtil.printPrettyJSONString(headerRequest));
	      logger.info(mensajeTransaccion + Constantes.PARAMETROSBODY + Constantes.SALTO_LINEA + requestPrint);
	      logger.info(mensajeTransaccion + Constantes.PARAMETROSOBLIGATORIOS);
	      BodyResponse bodyResponseValidacion = (BodyResponse) CastingMapper.validarParametrosEntrada(mensajeTransaccion,
	          httpHeaders, request);
	      if (Constantes.ZERO.equals(bodyResponseValidacion.getCodigoRespuesta())) {
	        logger.info(mensajeTransaccion + Constantes.VALIDACIONPARAMETROSCORRECTOS);
	        gestionarUsuarioResponse = gestionarUsuarioEverisService.gestionarUsuarioEveris(mensajeTransaccion, headerRequest,
	            request.getGestionarUsuarioEverisRequest(), propertiesExternos);
	        response.setGestionarUsuarioEverisResponse(gestionarUsuarioResponse);
	      } else {
	        logger.info(mensajeTransaccion + Constantes.VALIDACIONPARAMETROSINCORRECTOS);
	        responseAudit.setIdTransaccion(idtx);
	        responseAudit.setCodigoRespuesta(propertiesExternos.crearContratoIDF1Codigo);
	        responseAudit.setMensajeRespuesta(
	            propertiesExternos.crearContratoIDF1Mensaje + bodyResponseValidacion.getMensajeError());
	        gestionarUsuarioResponse.setResponseAudit(responseAudit);
	        response.setGestionarUsuarioEverisResponse(gestionarUsuarioResponse);
	      }

		} catch (Exception e) {
			logger.error(mensajeTransaccion + Constantes.ERROR_EXCEPTION + e.getMessage(), e);
			String errorLog = JAXBUtilitarios.getErrorTrace(e);
			response.setGestionarUsuarioEverisResponse(new GestionarUsuarioEverisResponseType());
			responseAudit.setCodigoRespuesta(propertiesExternos.idt3Codigo);
			responseAudit.setMensajeRespuesta(propertiesExternos.idt3Mensaje + Constantes.ESPACIO + e.getCause());
			gestionarUsuarioResponse.setResponseAudit(responseAudit);
			response.setGestionarUsuarioEverisResponse(gestionarUsuarioResponse);
			logger.info(Constantes.ERROR + errorLog);
		} finally {
			response.getGestionarUsuarioEverisResponse().getResponseAudit().setIdTransaccion(idtx);
			result = ClaroUtil.printPrettyJSONString(response);
			responsePrint = ClaroUtil.printPrettyJSONString(response);
			resJSON = Response.status(httpCode).entity(result).build();
			logger.info(mensajeTransaccion + Constantes.DEVOLVERRESPONSE + Constantes.SALTO_LINEA + responsePrint);
			logger.info(mensajeTransaccion + Constantes.SEPARADOR);
			logger.info(mensajeTransaccion + Constantes.FIN_METODO + Constantes.METODO_GESTIONAR_USUARIO
					+ Constantes.TIEMPO_TOTAL + (System.currentTimeMillis() - tiempoInicio) + Constantes.MILISEGUNDOS);
			logger.info(mensajeTransaccion + Constantes.SEPARADOR);
		}

		return resJSON;
	}

}
