package pe.com.claro.common.property;

public class Constantes {

  public static final String CHARSET = ";charset=UTF-8";
  public static final String APPLICATION_JSON = "application/json";
  public static final String RESOURCE = "/contrato";
  public static final String PATHBASE = "/contrato";
  public static final String VERSION = "1.0.0";

  public static final String NOMBRERECURSO = "claro-inte-gestionEveris";
  public static final String PATH_RESOURCE_DESCRIPCION = "Operaciones sobre contrato";

  public static final String PATH_METODO_CREAR_CONTRATO = "/crearContrato";
  public static final String METODO_CREAR_CONTRATO = "crearContrato";

  public static final String DATAAUDIT = "dataAudit";
  public final static String ID_TXT = " idTx=";
  public final static String MSG_ID = " msgid=";
  public static final String SEPARADOR = "-----------------------------------------------------------------------------";
  public final static String SALTO_LINEA = "\n";
  public static final String PARAMETROSENTRADA = " Parametros de entrada: ";
  public static final String PARAMETROSSALIDA = " Parametros de salida: ";
  public static final String PARAMETROSHEADER = " Header Request:";
  public static final String PARAMETROSBODY = "Body Request: ";
  public static final String PARAMETROSOBLIGATORIOS = "----0. Validar parametros obligatorios ------";
  public static final String VALIDACIONPARAMETROSCORRECTOS = " Validacion correcta de parametros de entrada";
  public static final String VALIDACIONPARAMETROSINCORRECTOS = " Parametros de entrada no cumple validacion";
  public static final String DEVOLVERRESPONSE = " Response a devolver: ";
  public static final String ERROR_EXCEPTION = "Error Exception: ";
  public final static String TIEMPO_TOTAL = " ] Tiempo total de proceso (ms): ";
  public final static String MILISEGUNDOS = " milisegundos.";
  public final static String REPLACEMETODO = "";
  public final static String REPLACEWS = "";

  // -------- WS
  public static final String CLASSTIMEOUTCONNECTION = "com.sun.xml.ws.connect.timeout";
  public static final String CLASSTIMEOUTREQUEST = "com.sun.xml.ws.request.timeout";

  // --------
  public static final String SEPARADORPUNTO = ".";
  public static final String FORMATOFECHACABECERA = "yyyy-MM-dd'T'HH:mm:ss'Z'";
  public static final String CODIGOFLAGACTIVO = "1";

  public static final String ID = "id";
  public static final String TEXTONULL = "null";
  public static final String TEXTOVACIO = "";

  public static final String FORMATOFECHADEFAULT = "dd/MM/yyyy HH:mm:ss";
  public static final String FORMATOFECHAISO = "yyyy-MM-dd'T'HH:mm:ss";
  public static final String FORMATOFECHA_ESTADO = "dd/MM/yyyy";
  // ----------------
  public static final String CODIGO200 = "200";
  public static final String CODIGO400 = "400";
  public static final String CODIGO404 = "404";
  public static final String MENSAJEOK = "OK";

  // --------Request---------
  public static final String IDTRANSACCION = "idTransaccion";
  public static final String MSGID = "msgid";
  public static final String USRID = "userId";
  public static final String TIMESTAMP = "timestamp";
  public static final String ACCEPT = "accept";
  public static final String IPAPLICACION = "ipAplicacion";
  public static final String NOMBAPLICACION = "nombreAplicacion";

  public static final String API = "api";
  public static final String CHAR_CORCHETE_IZQUIERDO = " [";
  public static final String CORCHETE = "]";
  // -----------------
  public static final String CONFIGPROPERTIES = "config.properties";
  public static final String SWAGGERJAXRSCONFIG = "SwaggerJaxrsConfig";
  public static final String URLSWAGGERJAXRSCONFIG = "/SwaggerJaxrsConfig";
  public static final String HTML5CORSFILTER = "HTML5CorsFilter";
  public static final String URLPATTERNS = "/api/*";
  public static final String ACCESSCONTROLALLOWORIGIN = "Access-Control-Allow-Origin";
  public static final String ACCESSCONTROLALLOWMETHODS = "Access-Control-Allow-Methods";
  public static final String ACCESSCONTROLALLOWHEADERS = "Access-Control-Allow-Headers";
  public static final String ASTERISCO = "*";
  public static final String METODOSPERMITIDOS = "GET, POST, DELETE, PUT";
  public static final String CONTENTTYPE = "Content-Type";
  // -----------------
  public static final String PROPERTIESINTERNOS = "config.properties";
  public static final String PROPERTIESEXTERNOS = ".properties";
  public static final String PROPERTIESKEY = "claro.properties";
  public static final String CONSTANTENOJNDI = "javax.persistence.PersistenceException";
  public static final String CONSTANTENOJNDIWS = "MessageBodyProviderNotFoundException";
  public static final String CONSTANTETIMEOUTWS = "java.net.SocketTimeoutException";
  public static final int NUM_ZERO = 0;
  public static final int NUM_UNO = 1;
  public static final String ZERO = "0";
  public static final String UNO = "1";
  public static final String MENOS_UNO = "-1";
  public static final String DOS = "2";
  public static final String TRES = "3";
  public static final String CUATRO = "4";
  public static final String CINCO = "5";
  public static final String SEIS = "6";
  public static final String SIETE = "7";
  public static final String OCHO = "8";
  public static final String NUEVE = "9";
  public static final int XXI = 21;
  public static final String DEFAULTENCONDIGPROPERTIES = null;
  public static final String DEFAULTENCONDINGAPI = null;
  public static final String FORMATO_FECHA_SP = null;
  public static final Object NULO = null;
  public static final String VALUE = " value ";
  // -----------------

  public final static int STATUS_TIME_OUT = 504;

  // Param DB
  public final static String TIEMPOTOTALPROCESO = "Tiempo TOTAL del proceso";
  public final static String CODIGOIDT1 = "contrato.valor.idt1.codigo";
  public final static String MENSAJEIDT1 = "contrato.valor.idt1.mensaje";
  public final static String CODIGOIDT2 = "contrato.valor.idt2.codigo";
  public final static String MENSAJEIDT2 = "contrato.valor.idt2.mensaje";

  // PENDIENTE
  public static final String PATH = "post/contrato/v1.0.0";
  public static final String BASEPATH = "/claro-post-contrato-resource/api";

  public static final String CONNECT_TIMEOUT = "com.sun.xml.internal.ws.connect.timeout";
  public static final String REQUEST_TIMEOUT = "com.sun.xml.internal.ws.request.timeout";
  public static final String TIMEOUT = "Timeout";
  public static final String EXCEPTION_WS_TIMEOUT_01 = "timed out";
  public static final String EXCEPTION_WS_TIMEOUT_02 = "SocketTimeoutException";
  public static final String EXCEPTION_WS_NO_DISPONIBLE_01 = "RemoteAccessException";
  public static final String EXCEPTION_WS_NO_DISPONIBLE_02 = "404";
  public static final String EXCEPTION_WS_NO_DISPONIBLE_03 = "WebServiceException";
  public static final String EXCEPTION_WS_NO_DISPONIBLE_04 = "ConnectException";
  public static final String WSEXCEPTION_OCURRIDA = "WSException ocurrida ";
  public static final String EXCEPTION_OCURRIDA = "Excepcion ocurrida: ";
  public static final String TEXTO_ERROR = " Ocurrio un error: ";
  public static final String ERROR = "[ERROR]: ";

  public final static String INICIO_METODO = " INICIO METODO: ";
  public final static String FIN_METODO = " FIN METODO: ";
  public final static String INICIO_ACTIVIDAD = " [Inicio Actividad: ";
  public final static String FIN_ACTIVIDAD = " [Fin Actividad: ";

  public static final String METODO = "$metodo";
  public static final String WS = "$ws";
  public static final String EX_CON_CORCHETE = "[$ex]";
  public static final String ESPACIO = " ";
  public static final String IGUAL = "=";
  public static final String DOSPUNTOS = ": ";
  public static final String TIMEOUTEXCEPTION = "Timeout";
  public static final String PERSISTENCEEXCEPTION = "javax.persistence.PersistenceException";
  public static final String HIBERNATEJDBCEXCEPTION = "The application must supply JDBC connections";
  public static final String GENERICJDBCEXCEPTION = "org.hibernate.exception";
  public static final String TRACE = " [TRACE] ";
  public static final String CODE = " [CODE]= ";
  public static final String MSG = " [MSG]= ";
  public static final String VARIABLE_DB = "$bd";
  public static final String VARIABLE_SP = "$sp";
  public static final String PERSISTENCE_CONTEXT_BSCS = "contrato.bscs";
  public static final String BUSCARPIPELINE = "\\|";
  public static final String EJECUTAR_SP = " Ejecutando SP: ";
  public static final String ERROR_EJECUCION_SP = " Error en la ejecucion del SP : ";
  public static final String ERROR_CONVERSION = " Error en la conversion ... ";
  public static final String ERROR_PARS_OBJ = "Error parseando object to xml: ";

  public static final String P_CUSTCODE = "p_custcode";
  public static final String P_COD_PROD = "P_COD_PROD";
  public static final String P_CURSOR = "P_CURSOR";
  public static final String P_RESULTADO = "P_RESULTADO";
  public static final String P_MSGERR = "P_MSGERR";
  public static final String SNCODE = "SNCODE";
  public static final String SPCODE = "SPCODE";
  public static final String CO_SER = "CO_SER";
  public static final String CARGO_FIJO = "CARGO_FIJO";
  public static final String PERIODOS = "PERIODOS";
  public static final String ESTADO = "ESTADO";
  public static final String P_TMCODE = "P_TMCODE";
  public static final String PI_MSISDN = "PI_MSISDN";
  public static final String PI_ICCID = "PI_ICCID";
  public static final String PO_SM_ID = "PO_SM_ID";
  public static final String PO_ERRNUM = "PO_ERRNUM";
  public static final String PO_ERRMSJ = "PO_ERRMSJ";
  public static final String P_CO_ID = "p_co_id";
  public static final String P_RESPUESTA = "p_respuesta";

  public static final String TIEMPO_TOTAL_SP = " Tiempo total de proceso del llamado del SP (ms): ";
  public static final String INVOCO_SP = " Se invoco con exito el SP";
  public static final String PARAMETROS_SALIDA = " Parametros de salida: ";

  public static final String CONTRACTSSEARCHDURL = "contract.serch.url";
  public static final String CONTRACTSSEARCHNOMBRE = "contract.serch.nombre";
  public static final String CONTRACTSSEARCHMETODO = "contract.serch.metodo";
  public static final String CONTRACTSSEARCHTIMEOUTCONNECT = "contract.serch.timeout.connect";
  public static final String CONTRACTSSEARCHTIMEOUTREQUEST = "contract.serch.timeout.request";

  public static final String CUSTOMERSSEARCHDURL = "customer.search.url";
  public static final String CUSTOMERSEARCHNOMBRE = "customer.search.nombre";
  public static final String CUSTOMERSEARCHMETODO = "customer.search.metodo";
  public static final String CUSTOMERSEARCHTIMEOUTCONNECT = "customer.search.timeout.connect";
  public static final String CUSTOMERSEARCHTIMEOUTREQUEST = "customer.search.timeout.request";

  public static final String CUSTOMERREADURL = "customer.read.url";
  public static final String CUSTOMERREADNOMBRE = "customer.read.nombre";
  public static final String CUSTOMERREADMETODO = "customer.read.metodo";
  public static final String CUSTOMERREADTIMEOUTCONNECT = "customer.read.timeout.connect";
  public static final String CUSTOMERREADTIMEOUTREQUEST = "customer.read.timeout.request";

  public static final String ADDRESSREADURL = "addres.read.url";
  public static final String ADDRESSREADNOMBRE = "addres.read.nombre";
  public static final String ADDRESSREADMETODO = "addres.read.metodo";
  public static final String ADDRESSREADTIMEOUTCONNECT = "addres.read.timeout.connect";
  public static final String ADDRESSREADTIMEOUTREQUEST = "addres.read.timeout.request";

  public static final String FORMATO_INTERNACIONAL = "51";
  public final static String EMPTY = "";
  public static final String CHAR_CORCHETE_DERECHO = " ] ";
  public final static String MILISEG = " milisegundos.";
  public static final String GUION = "-";
  public static final String PLATAFORMA = " Se valida plataforma: ";
  public static final String DATOS_ENTRADA = " Datos de Entrada: ";
  public static final String DATOS_SALIDA = " Datos de Salida: ";
  public static final String URL_WS_INVOCAR = " URL del Servicio a invocar: ";
  public static final String EXCEPCION_REST = " Excepcion ocurrida en la WS - REST ";
  public static final String P_PATH = "PATH: ";
  public static final String REQUEST_HEADER = " RequestHeader: ";
  public static final String REQUEST_BODY = " RequestBody: ";
  public static final String RESPONSE_HEADER = " ResponseHeader: ";
  public static final String RESPONSE_BODY = " ResponseBody: ";
  public static final String PALOTE = "|";
  public static final String PUNTO_COMA = ";";
  public static final String TEXTO_VACIO = "";
  public static final String BARRA = "\\|";

  public static final String ACTIVIDAD1 = "1.Obtener Servicio por codigo de servicio";
  public static final String ACTIVIDAD2 = "2.Obtener Servicio por tmcode";
  public static final String ACTIVIDAD3 = "3.Actualizar linea en BSCS 9";
  public static final String ACTIVIDAD4 = "4.Liberar recuersos de BSCS 9";
  public static final String ACTIVIDAD5 = "5.Crear Contrato en BSCS 7";
  public static final String ACTIVIDAD6 = "6.Validar contrato";
  public static final String TEXTOJNDI = " JNDI : ";
  public static final String TEXTOOWNER = " OWNER : ";
  public static final String TEXTOFUNCTION = " FUNCTION: ";
  public static final String TEXTOPAQUETE = " PAQUETE : ";
  public static final String TEXTOPROCEDURE = " PROCEDURE : ";
  public static final String CONSULTAEXITO = " Consulta Exitosa";
  public static final String BSCSJNDI = "db.bscs.jndi";
  public static final String CONSULTAINICIO = " Consulta Inicio : ";
  public static final String CALL = "call ";
  public static final String PARAMINPUT = " PARAMETROS [INPUT]: ";
  public static final String PARAMOUTPUT = " PARAMETROS [OUTPUT]: ";
  public static final String SQLTIMEOUTEXCEPTION = "SQLTIMEOUTEXCEPTION";
  public static final String TEXTOERRORTIMEOUT = "[Error De TimeOut en: ";
  public static final String TEXTOERRORDISPONIBILIDAD = "[Error De Disponibilidad en ] ";
  public static final String CHAR_CORCHETE_PUNTOS = ": [";
  public static final String EXCEPTIONTEXT = " Excepcion ocurrida en la BD {";
  public static final String NOMBRESP = "$sp";
  public static final String NOMBREJNDI = "$bd";
  public static final String TEXTOESPACIO = " ";
  public static final String PARAMOUTCURSOR = "PARAMOUTCURSOR";
  public final static String INI_CORCHETE1 = " 1. [";
  
  public static final String PATH_GESTION = "get/usuario/v1.0.0";
  public static final String BASEPATH_GESTION = "/claro-inte-gestionEveris-resource/api";
  public static final String PATH_RESOURCE_DESCRIPCION_GESTION = "Operaciones sobre el usuario everis";
  public static final String PATH_METODO_GESTIONAR_USUARIO = "/gestionarUsuario";
  public static final String METODO_GESTIONAR_USUARIO = "gestionarUsuario";
  
  public static final String ACTIVIDAD1_G = "1.Obtener número y tipo de documento de indentidad";
  public static final String ACTIVIDAD2_G = "2.Obtener Dispositivos asociados";
  public static final String ACTIVIDAD3_G = "3.Enviar mensaje a Procesar";
  public static final String ACTIVIDAD4_G = "4.Consultar Cuparticipante";
  public static final String TEXTO_USREAIDESA = " SYSTEM : ";
  public static final String TIMDEV_USREAIDESA = "db.timdev.usreaidesa";
  public static final String PERSISTENCE_CONTEXT_TIMDEV = "gestioneveris.timdev";
  public static final String COMA = ",";
  
  public static final String PO_MSISDN_EQUIP = "PO_MSISDN_EQUIP";
  public static final String PO_ICCID_EQUIP = "PO_ICCID_EQUIP";
  public static final String PO_IMSI_EQUIP = "PO_IMSI_EQUIP";
  public static final String PO_PLAN_EQUIP = "PO_PLAN_EQUIP";
  public static final String PO_IMEI_EQUIP = "PO_IMEI_EQUIP";
  
  public static final String PO_MSISDN_DISPASOC = "PO_MSISDN_DISPASOC";
  public static final String PO_ICCID_DISPASOC = "PO_ICCID_DISPASOC";
  public static final String PO_IMSI_DISPASOC = "PO_IMSI_DISPASOC";
  public static final String PO_IMEI_DISPASOC = "PO_IMEI_DISPASOC";
  public static final String PO_EID_DISPASOC = "PO_EID_DISPASOC";
  public static final String PO_EST_DISPASOC = "PO_EST_DISPASOC";
  public static final String PO_FEC_ACTIV = "PO_FEC_ACTIV";
  public static final String PO_DESC_SERV = "PO_DESC_SERV";
  public static final String P_CURSOR_EQUIP = "P_CURSOR_EQUIP";
  public static final String P_CURSOR_DISPASOC = "P_CURSOR_DISPASOC";

}