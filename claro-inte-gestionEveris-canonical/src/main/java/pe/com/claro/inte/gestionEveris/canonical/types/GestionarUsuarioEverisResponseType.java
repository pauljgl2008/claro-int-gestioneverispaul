package pe.com.claro.inte.gestionEveris.canonical.types;

import javax.validation.constraints.NotNull;

import pe.com.claro.common.property.ConstantesValidacion;

public class GestionarUsuarioEverisResponseType {

	@NotNull(message = ConstantesValidacion.NOT_NULL)
	private ResponseAuditType responseAudit;
	private ResponseDataType responseData;

	public ResponseAuditType getResponseAudit() {
		return responseAudit;
	}

	public void setResponseAudit(ResponseAuditType responseAudit) {
		this.responseAudit = responseAudit;
	}

	public ResponseDataType getResponseData() {
		return responseData;
	}

	public void setResponseData(ResponseDataType responseData) {
		this.responseData = responseData;
	}

}
